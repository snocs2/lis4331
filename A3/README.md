> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile Application Development

## Thomas Palmieri

### Assignment A3 Requirements:

1. Field to enter U.S. dollar amount : 1-100,000
2. Must Have toast notification
3. Must have Correct sign for currency
4. Must add background color
6. have SPLASH screen
7. Screenshots of application

#### README.md file should include the following items:

* Splash Screen picture
* Unpopulated UI pictures
* Toast notification pictures
* Converted Currency pictures
* pictures of skill checks

#### Assignment Screenshots:

*Screenshot of Splash Screen:

![](SplashScreen.PNG)

*Screenshot of Blank UI:

![](BlankCurrency.PNG)

*Screenshot of Toast notification*:

![](ToastAndroidPic.PNG)

*Screenshot of Android Studio - Converted Currency:

![](ConvertedCurrency.PNG)





*Screenshot of SS4:

![](SS4.PNG)

*Screenshot of SS5:

![](1_evenOrOdd.PNG)

![](2_evenOrOdd.PNG)

![](3_evenOrOdd.PNG)

![](4_evenOrOdd.PNG)

![](5_evenOrOdd.PNG)

![](6_evenOrOdd.PNG)

*Screenshot of SS6:

![](1_paint.PNG)

![](2_paint.PNG)

![](3_paint.PNG)

![](4_paint.PNG)

![](5_paint.PNG)

*Screenshot of SS7:

![](SS7.PNG)

*Screenshot of SS8:

![](1_triangle.PNG)

![](2_triangle.PNG)