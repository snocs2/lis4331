> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile Application Development

## Thomas Palmieri

### Assignment A5 Requirements:

1. Main screens and article link
2. Use rss feed
3. Add background
4. Display launcher icon

#### README.md file should include the following items:

* App screenshots
* website screenshot
* skill sets 13-15

#### Assignment Screenshots:

![](NewsPage1.png)

![](NewsPage2.png)

![](NewsWebsite.PNG)

Skill Sets: 

SKILL SET 14

![](Skillset14.PNG)

SKILL SET 15

![](Skillset15.PNG)