> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
> Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile Application Development

## Thomas Palmieri

### Assignment A2 Requirements:

1. Research drop-down menus in Android studio
2. Add background color/theme
3. Create launcher icon
4. Create Tip Calculator App
5. Screenshots of Application and Skillsets

#### README.md file should include the following items:

* Screenshot of unpopulated interface
* Screenshot of populated interface
* Screenshot of Launcher icon
* Screenshot of Skillsets

#### Assignment Screenshots:

*Screenshot of Unpopulated interface:

![](TipCalc1.png)

Screenshot of populated interface:

![](TipCalc2.png)

*Screenshot of SkillSet 1*:

![](SS1Done.PNG)

*Screenshot of SkillSet 2*:![](SS2Done.PNG)

*Screenshot of SkillSet 3*:![](SS3Done.PNG)

