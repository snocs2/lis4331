import java.util.Scanner;

public class Circle{

public static void main (String [] args)
{
	double diam = 0, cir =0, area=0;
System.out.println("Non-OOP program calculates diameter circumferene, and circle area.");
System.out.println("Must use Java's build-in PI constant, printf(), and formatted to 2 decimal places");
System.out.println("Must *only* permit numeric entry. \n");

Scanner input = new Scanner(System.in);
double radius=0.0;

System.out.println("Enter circle radius:");

//does not enter valid number
while(!input.hasNextDouble()){
System.out.println("Not valid Input!\n");
input.next();
System.out.print("Please try again. Enter cirlce radius: ");
}

radius = input.nextDouble();
diam= radius *2;
cir= 2*Math.PI*radius;
area=Math.PI*radius*radius;



System.out.printf("Circle diameter: %.2f",diam);
System.out.printf("\nCircumference: %.2f",cir);
System.out.printf("\nArea: %.2f \n",area);
}
}