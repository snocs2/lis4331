> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile Application Development

## Thomas Palmieri

### Assignment A4 Requirements:

1. include splash screen image
2.  include app title
3. include appropriate images
4. must use persistent data: shared preferences
5. widgets and images must be vertically and horizontally aligned
6. add background color and theme.
7. create display launcher icon

#### README.md file should include the following items:

* screen shot of splash screen
* screenshot of invalid screen
* screenshot of valid input
* skill checks 10 -12

#### Assignment Screenshots:

| Splash Screen   | Main Screen picture |
| --------------- | ------------------- |
| ![](Splash.png) | ![](Empty.PNG)      |

| Error Screen Page | Valid Input Page |
| ----------------- | ---------------- |
| ![](Error.png)    | ![](30.png)     |



**Skill set 10**

![](SS10.PNG)

**Skill set 11**



![](SS11_1.PNG)

![](SS11_2.PNG)

![](SS11_3.PNG)

**Skill Set 12**

![](SS12.PNG)