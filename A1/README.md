> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile Application Development

## Thomas Palmieri

### Assignment A1 Requirements:

1. Complete installs
2. Complete Bitbucket repo
3. complete hello world java
4. complete first application
5. complete contacts application

#### README.md file should include the following items:

* Git commands Definitions
* First Application pictures
* Contacts Application pictures
* Installation complete pictures
* bitbucket tutorial link

> #### Git commands w/short descriptions:

1. **git-init** - Create an empty Git repository or reinitialize an existing one
2. **git-status** - Show the working tree status
3. **git-add** - Add file contents to the index
4. **git-commit** - Record changes to the repository
5. **git-push** - Update remote refs along with associated objects
6. **git-pull** - Fetch from and integrate with another repository or a local branch
7. **git-diff** - Show changes between commits, commit and working tree, ect

#### Assignment Screenshots:

*Screenshot of git Installation:

![](ProofofGit.PNG)

*Screenshot of running java Hello*:

![](HelloWorldJava.PNG)

*Screenshot of Android Studio - My First App*:

| Hello World First picture    | Hello World Second picture   |
| ---------------------------- | ---------------------------- |
| ![](HelloWorld1stScreen.PNG) | ![](HelloWorld2ndScreen.PNG) |

*Screenshot of Android Studio - Contacts Application:

| First Contact Page      | Second Contact Page     |
| ----------------------- | ----------------------- |
| ![](ContactsAppPg1.PNG) | ![](ContactAppPage.png) |




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://ThomasPalmieri@bitbucket.org/snocs2/lis4331.git "Bitbucket Station Locations")

