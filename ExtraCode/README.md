> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Extra Code 

## Thomas Palmieri

#### README.md file should include the following items:

* bash script screenshots SQL and Shell scripts
* Weapon Script Pictures C#
* Source Code

#### Code Screenshots:

Working on Unity game in free time solo

Camera Movement

| Camera Movement with clamping C# | Source code                                     |
| -------------------------------- | ----------------------------------------------- |
| ![](CameraMovement.PNG)          | [SourceCode CameraMovement](CameraMovement.txt) |

| Enemy damage display Random Vector3 C# | Source Code                    |
| -------------------------------------- | ------------------------------ |
| ![](EnemyDisplay1.PNG)                 | [EnemyDamage](EnemyDamage.txt) |

| Weapon Pick Up and drop script C# | Weapon Manager script C#             |
| --------------------------------- | ------------------------------------ |
| ![](PickUp.PNG)                   | [PickUp/Drop](PickUp_DropScript.txt) |

![](WeaponManager.PNG)

**Bash and SQL** 

One question from my exams

![](Q07_bash.PNG)

![](q07_bash1.PNG)

![](q07_bash2.PNG)

![](q07_bash3.PNG)

**Another Example question**

![](q03_bash.PNG)

![](q03_bash1.PNG)