> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4331 Advanced Mobile App Development

## Thomas Palmieri

### LIS4331 Requirements:

*Course Work Links:*

1. [A1 README.md](A1/README.md "My A1 README.md file")
    - Install Java
    - Install Android Studio
    - Create My First App
    - Create Contacts App
    - Provide Screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial
    - Provide git command descriptions
2. [A2 README.md](A2/README.md "My A2 README.md file")
    - Research background color/theme
    - Research drop-down menus in Android Studio
    - Create launcher icon
    - Create Tip Calculator App
    - Screenshots of Application and Skillsets
3. [A3 README.md](A3/README.md "My A3 README.md file")
    - Research Splash Screen
    - Research radio buttons
    - Create launcher icon
    - Create Currency Converter Application
    - Screenshots of Application and Skillsets
4. [P1 README.md](P1/README.md "My P1 README.md file")
    - Research Playing audio
    - Include artists' images and media
    - Has to have buttons vertically and horizontally aligned
    - change theme
    - display launcher icon
    - Make buttons disappear when other active
5. [A4 README.md](A4/README.md "My A4 README.md file")
    - Create an app that calculates the total interest 
    - create launcher icon
    - display proper image depending on amount selected
    - use persistent data: shared preferences
6. [A5 README.md](A5/README.md "My A5 README.md file")
    - Create an app that uses RSS feed
    - display launcher icon
    - display proper titles
    - use different themes
7. [P2 README.md](P2/README.md "My P2 README.md file")
    - Research persistent data 
    - Use SQLite database
    - Has to have buttons vertically and horizontally aligned
    - change theme
    - display launcher icon
    - Display buttons