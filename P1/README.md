> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile Application Development

## Thomas Palmieri

### Assignment P1 Requirements:

1. Include Splash screen image, app title, and intro text.
2. Must Have artists' images and media
3. Images and buttons vertically and horizontally aligned
4. Must add background color and theme
6. display launcher icon

#### README.md file should include the following items:

* Splash Screen picture
* Default pictures
* Resume pictures
* Play pictures
* pictures of skill checks

#### Assignment Screenshots:

*Screenshot of Splash Screen:

![](SplashDONE.png)

*Screenshot of Default:

![](DefaultDONE.png)

*Screenshot of Resume:

![](ResumeDONE.PNG)

*Screenshot of Play:

![](playDONE.PNG)



*Screenshot of SS7:

![](SS7.PNG)

*Screenshot of SS8:

![](1_triangle.PNG)

![](2_triangle.PNG)