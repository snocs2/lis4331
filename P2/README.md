> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile Application Development

## Thomas Palmieri

### Assignment P2 Requirements:

1. Include Splash screen image
2. Use SQLite database
3. change theme
6. Proper database functions

#### README.md file should include the following items:

* Splash Screen picture
* User Entered pictures
* View database picture
* User changed pictures
* delete user picture

#### Assignment Screenshots:

**Splash**

![](Splash.PNG)

**User Entered**

![](NormalUser.png)

![](AddTomButton.png)

![](ViewAfterAdd.png)

**User Change Pictures**

![](ChangeUser.png)

![](UpdateButton.png)

![](Viewafterupdate.PNG)

**Delete pictures**

![](DeleteUser.png)

![](DeleteButton.png)

![](viewafterdelete.PNG)